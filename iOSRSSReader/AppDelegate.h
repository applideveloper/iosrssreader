//
//  AppDelegate.h
//  iOSRSSReader
//
//  Created by 荒木 敦 on 2014/09/26.
//  Copyright (c) 2014年 WishMatch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
//@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong,nonatomic) UITabBarController *tabBarController;


@end

