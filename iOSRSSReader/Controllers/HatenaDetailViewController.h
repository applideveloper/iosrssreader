//
//  HatenaDetailViewController.h
//  iOSRSSReader
//
//  Created by 荒木 敦 on 2014/09/26.
//  Copyright (c) 2014年 WishMatch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HatenaDetailViewController : UIViewController

@property (copy, nonatomic) NSDictionary *articleDictionary;

@end
