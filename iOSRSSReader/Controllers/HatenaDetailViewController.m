//
//  HatenaDetailViewController.m
//  iOSRSSReader
//
//  Created by 荒木 敦 on 2014/09/26.
//  Copyright (c) 2014年 WishMatch. All rights reserved.
//

#import "HatenaDetailViewController.h"
#import "HatenaDefine.h"

@interface HatenaDetailViewController () <UIWebViewDelegate>

@property (strong, nonatomic) UIWebView *webView;
//@property (strong, nonatomic) UIButton *prevButton;
//@property (strong, nonatomic) UIButton *nextButton;
//@property (strong, nonatomic) UIButton *goBackButton;
//@property (strong, nonatomic) UIButton *goForwardButton;

@end

@implementation HatenaDetailViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = self.articleDictionary[TITLE];
    
    int footerHeight = 46;
    
    NSURL *url = [NSURL URLWithString:self.articleDictionary[LINK]];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WINSIZE.width, WINSIZE.height-footerHeight)];
    self.webView.delegate = self;
    [self.webView loadRequest:req];
    self.webView.scalesPageToFit = YES;
    [self.view addSubview:self.webView];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.webView.frame.origin.y+self.webView.frame.size.height, WINSIZE.width, footerHeight)];
    footerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:footerView];
    
//    self.prevButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    self.prevButton.frame = CGRectMake(0, 0, 46, footerHeight);
//    [self.prevButton setTitle:@"←" forState:UIControlStateNormal];
//    [self.prevButton addTarget:self action:@selector(prevButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [footerView addSubview:self.prevButton];
//    
//    self.nextButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    self.nextButton.frame = CGRectMake(self.prevButton.frame.origin.x+self.prevButton.frame.size.width, self.prevButton.frame.origin.y, self.prevButton.frame.size.width, self.prevButton.frame.size.height);
//    [self.nextButton addTarget:self action:@selector(nextButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [self.nextButton setTitle:@"→" forState:UIControlStateNormal];
//    [footerView addSubview:self.nextButton];
    
//    UIButton *copyUrlButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    copyUrlButton.frame = CGRectMake(self.view.center.x, 0, WINSIZE.width/2, self.goBackButton.frame.size.height);
//    [copyUrlButton setTitle:@"URLをコピー" forState:UIControlStateNormal];
//    [copyUrlButton addTarget:self action:@selector(copyUrlButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [footerView addSubview:copyUrlButton];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
//    self.goBackButton.enabled = self.webView.canGoBack;
//    self.goForwardButton.enabled = self.webView.canGoForward;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    if (self.webView.isLoading) {
        [self.webView stopLoading];
    }
    
    [super viewWillDisappear:animated];
}

//- (void)goBackButtonTapped:(UIButton *)sender {
//    if (self.webView.canGoBack) [self.webView goBack];
//}
//
//- (void)goForwardButtonTapped:(UIButton *)sender {
//    if (self.webView.canGoForward) [self.webView goForward];
//}

- (void)copyUrlButtonTapped:(UIButton *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    //    NSLog(@"%@", UIPasteboardTypeListString);
    [pasteboard setValue:self.articleDictionary[LINK] forPasteboardType:UIPasteboardTypeListString[0]];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"たぶんコピー成功" delegate:nil cancelButtonTitle:@"おk" otherButtonTitles:nil];
    [alert show];
}



@end
