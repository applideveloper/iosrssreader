//
//  HatenaFunViewController.m
//  iOSRSSReader
//
//  Created by 荒木 敦 on 2014/09/26.
//  Copyright (c) 2014年 WishMatch. All rights reserved.
//

#import "HatenaFunViewController.h"
#import "HatenaParser.h"
#import "HatenaDefine.h"
#import "HatenaDetailViewController.h"

@interface HatenaFunViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *articles;
@property (strong, nonatomic) UITableView *tableView;

@end

@implementation HatenaFunViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Fun";
    self.articles = [HatenaParser parseResultWithCategoryName:@"fun"];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WINSIZE.width, WINSIZE.height) style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.articles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"ArticleCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = self.articles[indexPath.row][TITLE];
    
    if (indexPath.row%2) {
        cell.backgroundColor = [UIColor colorWithRed:0 green:1 blue:1 alpha:0.05];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES]; // ハイライトを解除
    
    HatenaDetailViewController *detailViewController = [[HatenaDetailViewController alloc] init];
    detailViewController.articleDictionary = self.articles[indexPath.row];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)refreshControlValueChanged:(UIRefreshControl *)sender {
    self.articles = [HatenaParser parseResultWithCategoryName:@"it"];
    [self.tableView reloadData];
    
    [sender endRefreshing];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

@end
