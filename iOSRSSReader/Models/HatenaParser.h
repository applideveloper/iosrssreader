//
//  HatenaParser.h
//  iOSRSSReader
//
//  Created by 荒木 敦 on 2014/09/26.
//  Copyright (c) 2014年 WishMatch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HatenaParser : NSObject

+ (NSArray *)parseResultWithCategoryName:(NSString *)categoryName;

@end
