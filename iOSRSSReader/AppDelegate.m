//
//  AppDelegate.m
//  iOSRSSReader
//
//  Created by 荒木 敦 on 2014/09/26.
//  Copyright (c) 2014年 WishMatch. All rights reserved.
//

#import "AppDelegate.h"
#import "HatenaITViewController.h"
#import "HatenaFunViewController.h"
#import "HatenaGameViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
//    HatenaViewController *viewController = [[HatenaViewController alloc] init];
//    self.navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
//    self.window.rootViewController = self.navigationController;
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    NSMutableArray *viewControllers = [[NSMutableArray alloc] init];
    HatenaITViewController *firstViewController = [[HatenaITViewController alloc] init];
    
    //それぞれNavigationControllerにセット
    //一個ずつ配列に追加していきます。
    UINavigationController *firstNavi = [[UINavigationController alloc] initWithRootViewController:firstViewController];
//    [firstNavi setNavigationBarHidden:YES]; //今回は最初のタブだけナビゲーションバーを非表示に設定。
    [viewControllers addObject:firstNavi];
    
    HatenaFunViewController *secondViewController = [[HatenaFunViewController alloc] init];
    UINavigationController *secondNavi = [[UINavigationController alloc] initWithRootViewController:secondViewController];
    secondNavi.title = @"Fun";
    [viewControllers addObject:secondNavi];
    
    HatenaGameViewController *thirdViewController = [[HatenaGameViewController alloc] init];
    UINavigationController *thirdNavi = [[UINavigationController alloc] initWithRootViewController:thirdViewController];
    thirdNavi.title = @"Game";
    [viewControllers addObject:thirdNavi];
    
    //最後にTabBarをセットして完了です。
    self.tabBarController = [[UITabBarController alloc] init];
    [self.tabBarController setViewControllers:viewControllers];
    
    self.window.rootViewController = self.tabBarController;
    
    [self.window makeKeyAndVisible];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
